import { ClimaAppPage } from './app.po';

describe('clima-app App', function() {
  let page: ClimaAppPage;

  beforeEach(() => {
    page = new ClimaAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
