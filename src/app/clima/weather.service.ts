import { Injectable } from '@angular/core';
import { ClimaItem} from "./clima-item";
import {Observable} from "rxjs/Observable";
import { Http } from '@angular/http';
import { ClimaComponent } from "./clima.component"

import 'rxjs/Rx'; 


@Injectable()
export class WeatherService {
 private climaItem: ClimaItem[] = [];
getWeatherItems(){
  return this.climaItem;
}

addWeatherItem(weatherItem :ClimaItem){
  this.climaItem.push(weatherItem);
}

  constructor(private _http: Http ) { }
getWeatherData(cityName: string){
  return this._http.get('http://api.openweathermap.org/data/2.5/weather?q=' + cityName
    + '&APPID=dc68a4b794d4f8216eed50b4190f6e2b&units=metric')
  .map(response => response.json())
  .catch(onerror => {
    console.error(onerror);
    return Observable.throw(onerror.json());
  });
}
}
