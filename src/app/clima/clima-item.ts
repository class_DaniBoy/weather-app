export class ClimaItem {
    public city: string;
    public info: string;
    public temp: number;

    constructor (city: string, info: string, temp: number){
        this.city = city;
        this.info = info;
        this.temp = temp;
    }
}