import { Component, OnInit } from '@angular/core';
import { ClimaItem } from "./clima-item"
import { WeatherService } from "./weather.service"
import {Observable} from "rxjs/Observable";
import {NgForm} from"@angular/forms";

@Component({
  selector: 'app-clima',
  templateUrl: './clima.component.html',
  styles: [],
  

})
export class ClimaComponent {
climaItem = [];
  constructor(private _weatherService: WeatherService ) { }
  
  onSubmit(form: NgForm) {
    this._weatherService.getWeatherData(form.value.cityName)
      .subscribe(
        data =>{
          const climaItem = new ClimaItem(data.name,
          data.weather[0].description, data.main.temp);
          this._weatherService.addWeatherItem(climaItem);

          }
      );    
 
    this.climaItem = this._weatherService.getWeatherItems();
  }

}
