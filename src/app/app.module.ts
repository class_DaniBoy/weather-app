import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ClimaComponent } from './clima/clima.component';
import { WeatherService } from './clima/weather.service';
import {ClimaItem} from "./clima/clima-item";
import { HeaderComponent } from './header.component';
import {routing} from "./app.routing";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ClimaComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [WeatherService],
  bootstrap: [AppComponent]
})
export class AppModule { }
