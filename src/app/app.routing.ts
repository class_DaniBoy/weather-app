import {Routes, RouterModule} from"@angular/router";
import {HomeComponent} from "./home/home.component";
import {ClimaComponent} from "./clima/clima.component";

const APP_ROUTES: Routes = [
    {path: '', redirectTo: '/home', pathMatch: 'full'},
     {path: 'home', component: HomeComponent},
     {path: 'clima', component: ClimaComponent}
];

export const routing = RouterModule.forRoot(APP_ROUTES);